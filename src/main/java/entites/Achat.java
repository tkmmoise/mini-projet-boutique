/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Achat implements Serializable {
    
    private Long id;
    private double remise;
    private LocalDateTime dateAchat;
    private Employe employe;
    private Client client;
    private ArrayList<ProduitAchete> listProduits ;
    
    public Achat() {
        this.remise =0;
        listProduits = new ArrayList<>();
    }

    //getters et setters
    public Long getId() {
        return id;
    }
    public double getRemise() {
        return remise;
    }
    public LocalDateTime getDateAchat() {
        return dateAchat;
    }
    public Employe getEmploye() {
        return employe;
    }
    public Client getClient() {
        return client;
    }
    public ArrayList<ProduitAchete> getListProduits() {
        return listProduits;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setRemise(double remise) {
        this.remise = remise;
    }
    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }
    public void setEmploye(Employe employe) {
        this.employe = employe;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public void setListProduits(ArrayList<ProduitAchete> listProduits) {
        this.listProduits = listProduits;
    }

    //autres methodes
    public boolean equals(Achat a) {
        return Objects.equals(this.id, a.getId());
    }

    public int hashCode() {
        return Objects.hash(id, remise, dateAchat, employe,client ,listProduits);
    }

    public String toString() {
        return "Achat{" +
                "id=" + id +
                ", remise=" + remise +
                ", dateAchat=" + dateAchat +
                ", employe=" + employe +
                ", client=" + client +
                '}';
    }

    public double getRemiseTotal() {
        double remiseTotale=0;
        for(ProduitAchete produitachete : listProduits){
            remiseTotale += produitachete.getRemise();
        }
        return remiseTotale;
    }

    public double getPrixTotal() {
        double prixTotale=0;
        for(ProduitAchete produitachete : listProduits){
            prixTotale += produitachete.getPrixTotal();
        }
        return prixTotale;
    }
    
    
}
